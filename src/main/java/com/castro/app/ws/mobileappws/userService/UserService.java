package com.castro.app.ws.mobileappws.userService;

import com.castro.app.ws.mobileappws.ui.model.request.UserDetailsRequestModel;
import com.castro.app.ws.mobileappws.ui.model.response.UserRest;

public interface UserService {
    UserRest createUser(UserDetailsRequestModel userDetails);
}
